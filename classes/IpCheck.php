<?php
class IpCheck
{
    private $access_key;
    private $_cache = [];
    function __construct()
    {
        $this->access_key = '50b805d4cc3c23ace9213cb03154ecc4';
    }
    function getJson($ip)
    {
        if ($ip == ' ') {
            return false;
        }
        if (isset($this->_cache[$ip])) {
            return $this->_cache[$ip];
        }

        $ch = curl_init('http://api.ipstack.com/' . $ip . '?access_key=' . $this->access_key . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json = curl_exec($ch);

        curl_close($ch);
        $decoder = json_decode($json, true);

        if (!isset($decoder['ip'])) {
            print_r("Error from checking IP");
            die();
        }

        return $this->_cache[$ip] = $decoder['continent_code'];
    }
}
