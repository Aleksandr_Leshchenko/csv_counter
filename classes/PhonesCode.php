<?php
class PhonesCode
{
    private $countryPhonesCode = [];
    function __construct()
    {
        $csvCountry = array_map('str_getcsv', file('contry_tree.csv'));
        foreach ($csvCountry as $key => $value) {
            if ($key > 0) {
                $this->countryPhonesCode[$value[1]] = $value[0];
            }
        }
    }


    public function getContinentByPhoneCode($code)
    {
        if (isset($this->countryPhonesCode[$code])) {
            return $this->countryPhonesCode[$code];
        }
    }
}
