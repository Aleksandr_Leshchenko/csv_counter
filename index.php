<table width="600">
    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">

        <tr>
            <td width="20%">Select file</td>
            <td width="80%"><input type="file" name="file" id="file" accept=".csv"/></td>
        </tr>

        <tr>
            <td>Submit</td>
            <td><input type="submit" name="submit" /></td>
        </tr>

    </form>
</table>
<?php
function __autoload($className)
{
    $className = str_replace("..", "", $className);
    require_once("classes/$className.php");
}

$rezut = [];
if (isset($_POST["submit"])) {

    if (isset($_FILES["file"])) {
        $mimes = array('text/csv');
        if (!in_array($_FILES['file']['type'], $mimes)) {
            echo ("Sorry, mime type not allowed");
            die();
        }
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {
            $phonesCode = new PhonesCode();
            $ipCheck = new IpCheck();
          
            if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    $ipchec=$ipCheck->getJson($data[4]);
                    $phones = $phonesCode->getContinentByPhoneCode(substr($data[3], 0, 3));
                    if ($ipchec == $phones) {
                        $rezult[$data[0]]['number_same_continent'] += 1;
                        $rezult[$data[0]]['total_duration_same_continent'] += $data[2];
                    }
                    $rezult[$data[0]]['totalDuration'] += $data[2];
                    $rezult[$data[0]]['all_calls'] += 1;
                }
                fclose($handle);
            }
        }
    }
}

if (!empty($rezult)) {
?>
    <table width="800">
        <tr>
            <th>CustomerId</th>
            <th>Number of calls within the same continent</th>
            <th>Total Duration of calls within the same continent</th>
            <th>Total number of all calls</th>
            <th>The total duration of all calls</th>
        </tr>
        <?php foreach ($rezult as $key => $value) { ?>
            <tr>
                <td>
                    <?php echo $key; ?>
                </td>
                <td align="center">
                    <?php echo ($value['number_same_continent'] >= 1 ? $value['number_same_continent'] : 0); ?>
                </td>
                <td align="center">
                    <?php echo ($value['total_duration_same_continent'] >= 1 ? $value['total_duration_same_continent'] : 0); ?> seconds</td>
                <td align="center">
                    <?php echo ($value['totalDuration'] >= 1 ? $value['totalDuration'] : 0); ?> seconds</td>
                <td align="center">
                    <?php echo $value['all_calls']; ?>
                </td>

            </tr>
        <?php } ?>

    </table>
<?php
}
?>